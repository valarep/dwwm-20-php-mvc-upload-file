<?php
namespace MyProject\Model\Classes;

use MyProject\Kernel\AbstractObject;

class Sample extends AbstractObject
{
    private $id;
    protected function getId() { return $this->id; }
    protected function setId($value) { $this->id = $value; }
    
    private $name;
    protected function getName() { return $this->name; }
    protected function setName($value) { $this->name = $value; }
}