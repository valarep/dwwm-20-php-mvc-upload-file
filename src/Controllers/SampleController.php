<?php
namespace MyProject\Controllers;

use MyProject\Kernel\Route;
use MyProject\Kernel\Router;
use MyProject\Kernel\View;
use MyProject\Model\Classes\Sample;

class SampleController
{
    public static function route()
    {
        // Routage secondaire
        $router = new Router();
        $router->addRoute(new Route("/", "MyProject\\Controllers\\SampleController", "homeAction"));
        $router->addRoute(new Route("/upload", "MyProject\\Controllers\\SampleController", "uploadAction"));

        $route = $router->findRoute();

        if ($route)
        {
            $route->execute();
        }
        else
        {
            // Erreur 404
            echo "Page not found";
        }
    }

    public static function homeAction()
    {
        $title = "My Project";

        View::setTemplate("home.tpl");
        View::bindVar("title", $title);
        View::display();
    }

    public static function uploadAction()
    {
        $src = $_FILES["fileToUpload"]["tmp_name"];
        
        $origin_filename = basename($_FILES["fileToUpload"]["name"]);
        
        $exploded_filename = explode(".", $origin_filename);
        // Est ce que le fichier à une extension ???
        if (count($exploded_filename) > 1)
        {
            // oui il en a une !
            $last_part = array_pop($exploded_filename);
            $ext = "." . $last_part;
        }
        else
        {
            // Non il n'en a pas le bougre !
            $ext = "";
        }
        
        $upload_dir = "uploads/";
        
        $check = true;
        
        // Déterminer le MIME type du fichier
        $mime = mime_content_type($src);
        
        $authorized_mimes = [
            'image/jpeg',
            'image/png',
            'image/gif',
            'image/tiff',
        ];
        
        // Test 1
        // Est ce que le MIME récupérer correspond ???
        if (! in_array($mime, $authorized_mimes, true))
        {
            // NON c'est un FAKE !!!
            $message = "Erreur: tu es méchant !";
            $check = false;
        }
        
        // Test 2
        // Est ce que le fichier est trop gros ??
        $max_size = 500 * 1024; // bytes (octets) 500 ko = 500 * 1024 o
        if ($check && filesize($src) > $max_size)
        {
            // TROP GROS !!!
            $message = "Erreur : Trop gros ! (taille max : 500ko)";
            $check = false;
        }
        
        // Test 3
        // Génération d'un nom unique ?
        $datetime = date("YmdHis");
        $sha1 = sha1_file($src);
        $sha1 = substr($sha1, 0, 7);
        
        $filename = $datetime . $sha1 . $ext;
        $dest = $upload_dir . $filename;
        
        // Est ce qu'il y a un fichier du même nom qui existe déjà ??
        if ($check && file_exists($dest))
        {
            // oui y en a un !!!
            $message = "Erreur : y a déjà un fichier du même nom !";
            $check = false;
        }
        
        if ($check)
        {
            // Tout va bien :)
            move_uploaded_file($src, $dest);
            $message = "Fichier téléchargé :)";
        }
        
        View::setTemplate("upload_result.tpl");
        View::bindVar("message", $message);
        View::display();

    }
}