<?php
namespace MyProject\Kernel;

use Exception;

class AbstractObject
{
    public function __get($name)
    {
        // Check property
        $class = get_class($this);
        if (! property_exists($class, $name))
        {
            // property not found
            throw new Exception("Property '{$name}' not found in class '{$class}'.");
        }
        else
        {
            // property found
            // Check method
            $method = "get" . ucfirst($name);
            if (! method_exists($this, $method))
            {
                // method nor found
                throw new Exception("Get method not found for the property '{$name}' in class '{$class}'.");
            }
            else
            {
                // Method exists
                return $this->$method();
            }
        }
    }

    public function __set($name, $value)
    {
        // Check property
        $class = get_class($this);
        if (! property_exists($class, $name))
        {
            // property not found
            throw new Exception("Property '{$name}' not found in class '{$class}'.");
        }
        else
        {
            // property found
            // Check method
            $method = "set" . ucfirst($name);
            if (! method_exists($this, $method))
            {
                // method nor found
                throw new Exception("Set method not found for the property '{$name}' in class '{$class}'.");
            }
            else
            {
                // Method exists
                $this->$method($value);
            }
        }
    }
}