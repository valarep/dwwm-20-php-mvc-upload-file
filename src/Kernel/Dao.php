<?php

namespace MyProject\Kernel;

use PDO;
use PDOException;

class Dao
{
    private static $host = "127.0.0.1";
    private static $port = "3306";
    private static $database = "database";
    private static $charset = "UTF8";
    private static $user = "user";
    private static $password = "password";
    private static $connection;

    public static function open()
    {
        $dsn = "mysql:" .
                "host=" . self::$host . ";" .
                "port=" . self::$port . ";" .
                "dbname=" . self::$database . ";" . 
                "charset=" . self::$charset . ";";
        
        try {
            self::$connection = new PDO($dsn, self::$user, self::$password);
            return self::$connection;
        } catch (PDOException $ex) {
            View::setTemplate("error");

            View::bindVar("code", "1");
            View::bindVar("message", "Database Fatal Error.");

            View::display();
            die();
        }
    }

    public static function close()
    {
        self::$connection = null;
    }
}